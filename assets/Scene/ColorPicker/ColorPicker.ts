/**
 * @description 取色器 获取某一点的RGB值
 */
const {ccclass, property} = cc._decorator;

@ccclass
export default class ColorPicker extends cc.Component {

    @property(cc.Camera)
    cameraGraphic: cc.Camera = null;
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        let render = new cc.RenderTexture();
        render.initWithSize(cc.winSize.width,cc.winSize.height);
        this.cameraGraphic.targetTexture = render;
        this.cameraGraphic.render(this.node);
        
        this.node.on(cc.Node.EventType.TOUCH_END, this.touchEndEvent, this);
    }

    touchEndEvent(event){
        let pos = event.getLocation();
        var color = this.getGraphicsData(pos);
        cc.log("=====color======",color);
    }

    getGraphicsData(point:cc.Vec3){
        let Unit8 = new Uint8Array(4);
        Unit8 = this.cameraGraphic.targetTexture.readPixels(Unit8,point.x,point.y,1,1);
        return Unit8;
    }

    // update (dt) {}
}
